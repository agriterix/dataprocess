# Agriterix post process script

This script process [simulation](https://forgemia.inra.fr/agriterix/simulator) data in order to be used 
on a [map view](https://forgemia.inra.fr/agriterix/datamap). 

The processing can be done either in the [Jupyter notebook](process.ipynb) or the plain Python 3 [script](process.py) (converted by the command `jupyter nbconvert --to script [notebookfilename].ipynb`). [ages.ipynb](ages.ipynb) follows the same way.

Python packages requirements are located in [pip-requires.txt](pip-requires.txt).