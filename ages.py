#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd


# In[60]:


ages = pd.read_csv("data/ages_classes.csv")
evolutions = {}


# In[61]:


evolutions["18-30"] = ages[ages.columns[1:14]].transpose().sum()
evolutions["31-50"] = ages[ages.columns[14:39]].transpose().sum()
evolutions["50+"] = ages[ages.columns[40:-1]].transpose().sum()


# In[79]:


evolutions_df = pd.concat([evolutions[i] for i in evolutions.keys()], axis=1)
evolutions_df.columns = evolutions.keys()
evolutions_df


# In[80]:


evolutions_df.to_csv('data/agec_evolutions.csv')

