#!/usr/bin/env python
# coding: utf-8

# In[2]:


import geopandas as gpd
import pandas as pd
import cbor2
import numpy as np
import yaml


# In[3]:


with open("data/INDEX.yaml") as fh:
    data = yaml.load(fh, Loader=yaml.FullLoader)

rpg = gpd.read_file(data["patches_before"]["file"])
hexagons = gpd.read_file(data["hex3400"]["file"])
hexagons = hexagons.drop(["left", "bottom", "right", "top"], axis=1)

with open(data["patches_after"]["file"], "rb") as fp:
    patches_after_cbor = cbor2.load(fp)

patches_after_path = "data/patches.csv"
patches_after_csv = open(patches_after_path, "w")
for line in patches_after_cbor:
    towrite = str(line)[1:-1]
    towrite = towrite.replace("'", "")
    towrite = towrite.replace(", ", ",")
    patches_after_csv.write(towrite + "\n")
patches_after_csv.close()

patches = pd.read_csv(patches_after_path)
rpg.ID_PARCEL = pd.to_numeric(rpg.ID_PARCEL)
rpg = rpg.merge(patches, on="ID_PARCEL")

# codecultco → culture, id_expl → id_exploit

hex_rpg = gpd.sjoin(hexagons, rpg, op="contains")
hex_rpg = hex_rpg.drop_duplicates(subset=["index_right"])
hex_rpg = hex_rpg.reset_index()


# In[4]:


hexagg = gpd.GeoDataFrame()

surface_cultures_old = gpd.GeoDataFrame()
surface_cultures_old = (
    hex_rpg[["index", "codecultco", "surf_ilot"]].groupby(["index", "codecultco"]).sum()
)
surface_cultures_old.reset_index(inplace=True)
surface_cultures_old = surface_cultures_old.pivot_table(
    values="surf_ilot",
    index=surface_cultures_old["index"],
    columns="codecultco",
    aggfunc="first",
)
surface_cultures_old = surface_cultures_old.fillna(0)

surface_cultures_new = gpd.GeoDataFrame()
surface_cultures_new = (
    hex_rpg[["index", "culture", "surf_ilot"]].groupby(["index", "culture"]).sum()
)
surface_cultures_new.reset_index(inplace=True)
surface_cultures_new = surface_cultures_new.pivot_table(
    values="surf_ilot",
    index=surface_cultures_new["index"],
    columns="culture",
    aggfunc="first",
)
surface_cultures_new = surface_cultures_new.fillna(0)
# below TODO diff


# In[5]:


hex_rpg


# In[6]:


surface_cultures_delta = surface_cultures_new - surface_cultures_old
surface_cultures_delta


# In[8]:


nb_parcelles = hex_rpg[["index", "index_right"]].groupby("index").count()
nb_parcelles = nb_parcelles.rename(columns={"index": "nb_parcelles"})

nb_expl_old = hex_rpg.groupby("index")["id_expl"].unique()
nb_expl_new = hex_rpg.groupby("index")["id_exploit"].unique()

nb_expl_new = nb_expl_new.transform(np.size)
nb_expl_old = nb_expl_old.transform(np.size)
nb_expl_delta = nb_expl_new - nb_expl_old

surface_totale = hex_rpg[["index", "surf_ilot"]].groupby("index").sum()
surface_totale = surface_totale.rename(columns={"surf_ilot": "surf_totale"})

geometry = (
    hex_rpg.groupby("index")
    .first()
    .drop(
        [
            "index_right",
            "codecultco",
            "surf_ilot",
            "surface",
            "pratique",
            "production_x",
            "production_y",
            "culture",
            "id_exploit",
            "surface",
            "id_expl",
        ],
        axis=1,
    )
)

surface_cultures_delta["surf_totale"] = surface_totale["surf_totale"]
surface_cultures_delta["nb_expl_delta"] = nb_expl_delta
surface_cultures_delta["nb_expl_new"] = nb_expl_new
surface_cultures_delta["nb_expl_old"] = nb_expl_old

hexagg = gpd.GeoDataFrame(surface_cultures_delta, geometry=geometry.geometry)
hexagg


# In[ ]:


hexagg = hexagg.set_crs(epsg=2154)
hexagg = hexagg.to_crs(epsg=3857)
hexagg.to_file("data/hexagg.geojson", driver="GeoJSON")

